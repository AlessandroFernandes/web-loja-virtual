function numeroReais(numero){return "R$ " + numero.toFixed(2).replace(".",",");}

function numeroFinal(numero2){ 
var preparar = numero2.replace("R$ ","").replace(",",".");
var numeroPreparar = parseFloat(preparar).toFixed(2);
return numeroPreparar;
}


var $input_quantidade = document.querySelector("#qt");
var $output_total = document.querySelector("#total");


$input_quantidade.oninput = calculoTotal;

function calculoTotal(){

	var quantidade = $input_quantidade.value;

	var valorTexto = document.querySelector("#preco").textContent;
	var valorNumero = numeroFinal(valorTexto);

	var total = numeroReais(quantidade * valorNumero);
	$output_total.value = total;
}
