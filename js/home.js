

var banners = ["image/big.jpg", "image/big2.jpg"];
var bannerAtual = 0;

function trocaBanner(){
	bannerAtual = (bannerAtual + 1) % 2;
	document.querySelector('.destaque img').src = banners[bannerAtual];
}
var timer = setInterval(trocaBanner, 4000);

var controle = document.querySelector('.pause');


controle.onclick = function(){
	if (controle.className == 'pause') {
		clearInterval(timer);
		controle.className = 'play';
	}else{
		timer = setInterval(trocaBanner, 4000);
		controle.className = 'pause';
	}
	return false;
};
$('.novidades, .mais-vendidos').addClass('painel-compacto');

$('.novidades button, .mais-vendidos button').click(function() {
	$('.novidades, .mais-vendidos').removeClass('painel-compacto');
});


