<!DOCTYPE html>
<html>
<head>
	<title>Teste de Mercado</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<link rel="stylesheet" type="text/css" href="css/base2.css">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/mobile.css" media="(max-width: 939px)">
	<script type="text/javascript" src="js/jquery.js"></script>
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans|Bad+Script">
	<!--<script src="js/less.js"></script>-->

<style type="text/css">
	
body{
	font-family: 'PT Sans', sans-serif;
}
.painel h2{
	font-family: 'Bad Script', sans-serif;
}
	
</style>
	
</head>
<body>
	<header class="container">
		<h1><img src="image/jiulogo.jpg" alt="Jiu Jitsu Magazine"></h1>

		<p class="sacola">
		Nenhum item na sacola de compras
		</p>
		<nav class="menu-opcoes">
		<ul>
			<li><a href="#">Sua Conta</a></li>	
			<li><a href="php/produto.php">Lista de Desejos</a></li>
			<li><a href="#">Cartão Fidelidade</a></li>
			<li><a href="#">Sobre</a></li>
			<li><a href="#">Ajuda</a></li>
		</ul>
		</nav>
		</header>



		<div class="container destaque">
		<section class="busca">
			
			<form>
			<h2>Busca</h2>
				<input type="search"></input>
				<input type="image" src="image/lupa.png" class="lupa"></input>
			</form>
		</section><!-- fim da classe busca -->

		<section class="menu-departamentos">
			<h2>Departamentos</h2>
				<nav>
					<ul>
						<li><a href="#">Blusas e camisas</a>
							<ul>
								<li><a href="#">Manga Curta</a></li>
								<li><a href="#">Manga Longa</a></li>
								<li><a href="#">Regata</a></li>
								<li><a href="#">Camisa Machao</a></li>
							</ul>
						</li>
						<li><a href="#">Calças</a></li>
						<li><a href="#">Saias</a></li>
						<li><a href="#">Vestidos</a></li>
						<li><a href="#">Sapatos</a></li>
						<li><a href="#">Bolsas e Carteiras</a></li>
						<li><a href="#">Acessórios</a></li>
					</ul>
				</nav>
		</section><!-- fim da classe menu-departamentos -->
		
		
		<img src="image/big2.jpg" alt="Promoção: kimonos Dragão" class="destaque">
		<a href="#" class="pause"></a>
		

		<h1> Teste </h1>
		</div>
				<div class="container paineis">
					<section class="painel novidades">
						<h2>Novidades</h2>
						
							<ol>
							<?php
							$conexao = mysqli_connect("127.0.0.1", "sandroex","sandro","sandroex");
							$dados = mysqli_query($conexao, "SELECT * FROM produtos ORDER BY data DESC LIMIT 0,12");

							while ($produto = mysqli_fetch_array($dados)):
							?>
							<li>
								<a href="php/produto.php?id=<?= $produto['id'] ?>">	
								
									<img src="image/<?= $produto['id'] ?>.jpg" alt="<?= $produto['produto']?>">
									<figcaption><?= $produto['produto']?> por R$<?= $produto['preco']?></figcaption>
								</a>
							</li>
								<?php endwhile; ?>
								</ol>
								<button type="button">Mostra mais</button>
					</section>

					<section class="painel mais-vendidos">
						<h2>Mais Vendidos</h2>
								<ol>
												<li>
									<a href="#">
											<img src="image/kimonoD.jpg" alt="Kimono Dragão">
											<figcaption>Kimono Dragão por R$ 120,00
											</figcaption>
									</a>	
								</li>
								<li>
									<a href="#">
									     	<img src="image/kimonoD.jpg" alt="Kimono Judo">
											<figcaption>Kimono Judo por R$ 200,00</figcaption>	
									</a>
								</li>
								<li>
									<a href="#">
											<img src="image/kimonoD.jpg" alt="Kimono Adulto">
											<figcaption>Kimono Adulto por R$ 150,00</figcaption>
									</a>
								</li>
								<li>
									<a href="#">
											<img src="image/kimonoD.jpg" alt="Kimono Casca Grossa">
											<figcaption>Kimono Casca Grossa por R$ 250,00</figcaption>
									</a>
								</li>
								<li>
									<a href="#">
									    	<img src="image/kimonoD.jpg" alt="Kimono Venum">
											<figcaption>Kimono Venum por R$ 300,00</figcaption>
									</a>
								</li>
								<li>
									<a href="#">
											<img src="image/kimonoD.jpg" alt="Kimono Kross">
											<figcaption>Kimono Kros por R$ 120,00</figcaption>
									</a>
								</li>

								<li>
									<a href="#">
											<img src="image/kimonoD.jpg" alt="Kimono Casca Grossa">
											<figcaption>Kimono Casca Grossa por R$ 250,00</figcaption>
									</a>
								</li>
								<li>
									<a href="#">
									    	<img src="image/kimonoD.jpg" alt="Kimono Venum">
											<figcaption>Kimono Venum por R$ 300,00</figcaption>
									</a>
								</li>
								<li>
									<a href="#">
											<img src="image/kimonoD.jpg" alt="Kimono Kross">
											<figcaption>Kimono Kros por R$ 120,00</figcaption>
									</a>
								</li>
								</ol>
								<button type="button">Mostra mais</button>

					</section>
				</div>

					<footer>
						<div class="container">
						<img src="image/jiulogo.jpg" alt="Imagem Rodape">

							<ul class="social">
								<li><a href="https://facebook.com">Facebook</a></li>
								<li><a href="https://twitter.com">Twitter</a></li>
								<li><a href="https://google.com">Google+</a></li>
							</ul>

						</div>
					</footer>
					<script type="text/javascript" src="js/home.js"></script>
</body>
</html>
