<!DOCTYPE html>
<html>
<head>
	<title>Checkout Loja</title>
	<meta charset="UTF-8">
	<meta name="wiewport" content="width=device-width">
	<script type="text/javascript" src="../js/jquery.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<script type="text/javascript" src="../js/inputmask.js"></script>	
	<script type="text/javascript" src="../js/jquery.inputmask.js"></script>
	
<style type="text/css">
.form-control:invalid{
	border: 1px solid #cc0000;
}	
.navbar{
	margin:0;
}
.body{
	padding-top: 70px;
}
.navbar img{

	width: 3em;
}

</style>
</head>

<body>

	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="navbar-header">
			<a  class="navbar-brand" href="../index.html">
			<img src="../image/jiulogo.jpg">
			</a>

			<button class="navbar-toggle glyphicon glyphicon-align-justify" type="button" data-target=".navbar-collapse" data-toggle="collapse"  > Menu</button>
			
		</div>
		<ul class="nav navbar-nav collapse navbar-collapse">
			<li><a href="#">Sobre</a></li>
			<li><a href="#">Ajuda</a></li>
			<li><a href="#">Perguntas Frequentes</a></li>
			<li><a href="#">Entre em Contato</a></li>
		</ul>
	</nav>

	<div class="jumbotron container">		
	
<h1>Ótima Escolha!</h1>
<p>Obrigado por comprar na "Loja Teste!"
Preecha seus dados para efetuar a compra.</p>
		</div>
		
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-lg-3">
	<div class="panel panel-default">
	<div class="panel-heading">
			<h2 class="panel-title">Sua Compra</h2>
			</div>

			<div class="panel-body">
			<img src="../image/<?php print $_POST["id"]?>-<?php print $_POST["cor"] ?>.jpg" class="img-thumbnail img-responsive hidden-xs">
			
			<dl>
				<dt>Nome do Produto</dt>
				<dd><?php print ($_POST['nome'])?></dd>

				<dt>Preço</dt>
				<dd id="preco"><?php print ($_POST['preco'])?></dd>
				
				<dt>Cor</dt>
				<dd><?php print ($_POST['cor'])?></dd>

				<dt>Tamanho</dt>
				<dd><?php print ($_POST['tamanho'])?></dd>

				
				</dl>

				<div class="form-group">
					<label for="qt">Quantidade</label>
					<input id="qt" class="form-control" name="qt" type="number"  min="0" max="99" value="1">
				</div>
				<div class="form-group">
					<label for="total">Total</label>
					<output for="qt valor" id="total" class="form-control">
						<?php print $_POST['preco'] ?>
					</output>
				</div>
				</div>
		</div>
		</div>

				<form class="col-sm-8 col-lg-9">
					<div class="row">
						<div class="col-md-6">
					<fieldset>
						<legend>Dados Pessoais</legend>

						<div class="form-group">
							<label for="nome"> Nome Completo</label>
							<input type="text" class="form-control" id="nome" name="nome" autofocus></input>
						</div>

						<div class="form-group">
							<label for="email">Email</label>

							<script>
							/*document.querySelector('input[type=email]').oninvalid = function(){
								this.setCustomValidity("");
								if (!this.validity.valid) {
									this.setCustomValidity("Email inválido");
								}

							};*/
							</script>

							<div class="input-group">
								<span class="input-group-addon">@</span>
								<input type="email" class="form-control" id="email" name="email" placeholder="email@exemplo.com.br"></input>
							</div>
						</div>
						
						

						<div class="form-group">
							<label for="cpf">CPF</label>
							<input type="text" class="form-control" id="cpf" name="cpf" placeholder="111.111.111-11" required></input>
						</div>
						
						<div class="checkbox">
							<label>
								<input type="checkbox" value="sim" name="spam"  ckecked></input>
							Quero receber spam de Novidades
							</label>
						</div>
					</fieldset>
						</div>
						<div class="col-md-6">
					<fieldset>
						<legend>Cartão de Crédito</legend>
					
					<div class="form-group">
						<label for="numero-cartao"> Número - CVV</label>
						<input type="text" class="form-control" id="numero-cartao" name="numero-cartao" ></input>
					</div>
				

				<script>$(function(){
							$("#cpf").inputmask("999.999.999-99");
							$("#numero-cartao").inputmask("9999 9999 9999 9999 - 999")
							});</script>


					<div class="form-group">
						<label for="bandeira-cartao">Bandeira</label>
						<select name="bandeira-cartao" id="bandeira-cartao" class="form-control">
							<option value="master">MasterCard</option>
							<option value="visa">Visa</option>
							<option value="amex">American Express</option>
						</select>
					</div>				

					<div class="form-group">
						<label for="validade-cartao">Validade</label>
						<input type="month" class="form-control" id="validade-cartao" name="validade-cartao"></input>
					</div>

					</fieldset>
					</div>
					<button type="submit" class="btn btn-primary btn-lg pull-right">
					<span class="glyphicon glyphicon-thumbs-up"></span>
						Confirmar Pedido
					</button>
				</form>
				</div>
			</div>
			</div>
			</div>
			

</div>

<script type="text/javascript" src="../js/bootstrap.js"></script>
<script type="text/javascript" src="../js/converteMoeda.js"></script>



</body>
</html>