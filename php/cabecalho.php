<!DOCTYPE html>
<html>
<head>
	<title><?= $title_produtos ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	
	<script type="text/javascript" src="../js/jquery.js" ></script>
	<script type="text/javascript" src="../js/bootstrap.js"></script>

	<link rel="stylesheet" type="text/css" href="../css/base2.css">
	<link rel="stylesheet" type="text/css" href="../css/reset.css">
	<link rel="stylesheet" type="text/css" href="../css/produto.css">
	<link rel="stylesheet" type="text/css" href="../css/mobile.css" media="(max-width: 939px)">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<!--<script src="../js/less.js"></script>-->
	<?php print @$stilo_css;  ?>
</head>
<body>
	<header class="container">
		<a href="../index.html"><h1><img src="../image/jiulogo.jpg" alt="Jiu Jitsu Magazine"></h1></a>

		<p class="sacola">
		Nenhum item na sacola de compras
		</p>
		<nav class="menu-opcoes">
		<ul>
			<li><a href="#">Sua Conta</a></li>	
			<li><a href="php/produto.php">Lista de Desejos</a></li>
			<li><a href="#">Cartão Fidelidade</a></li>
			<li><a href="#">Sobre</a></li>
			<li><a href="#">Ajuda</a></li>
		</ul>
		</nav>
		</header>
