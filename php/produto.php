	<?php
	
	$conexao = mysqli_connect("127.0.0.1","sandroex","sandro","sandroex");
	$dados = mysqli_query($conexao, "SELECT * FROM produtos WHERE id = $_GET[id]");
	$produto = mysqli_fetch_array($dados);

			$title_produtos = $produto['produto'];
			include ("cabecalho.php");
	?>

	
	<div class="produto-back">	
		<div class="container">
			
			<div class="produto">
		<h1><?= $produto['produto'] ?></h1>
		<p>por apenas R$ <?= $produto['preco'] ?></p>

		<form action="checkout.php" method="POST" >
			<fieldset class="cores">
				<legend>Escolha a cor:</legend>
				
				<input type="radio" name='cor' id="preto"  value="preto" checked="true">
				<label for="preto">
					<img src="../image/<?= $produto['id']?>-preto.jpg" alt="kimono preto adulto">
				</label>

				<input type="radio" name="cor"  id="verde" value="verde">
				<label for="verde">
					<img src="../image/<?= $produto['id']?>-verde.jpg" alt="kimono verde adulto">
				</label>

				<input type="radio" name="cor" id="branco" value="branco">
				<label for="branco">
					<img src="../image/<?= $produto['id']?>-branco.jpg" alt="kimono branco adulto">
				</label>
			</fieldset>
			<fieldset class="tamanhos">
			<legend>Escolha o Tamanho:</legend>
			
			<input type="range" min="36" max="46" value="42" step="2" name="tamanho" id="tamanho" class="taman"></input>

			<output  for="tamanho" name="tamanhodoproduto"id="tamanhodoproduto">42</output>
			</fieldset>

				<input type="submit" class="comprar" value="Comprar">
				<input type="hidden" name="id" value="<?= $produto['id']?>">
				<input type="hidden" name="nome" value="<?= $produto['produto']?>">
				<input type="hidden" name="preco" value="R$ <?= $produto['preco']?>">
		</form>
		</div>
		<script>
		$('[name=tamanho]').on('input', function() {
			$('[name=tamanhodoproduto').val(this.value);
		});	
		</script>

		<div class="detalhes">
			<h2>Detalhes do produto</h2>
			<p><?= $produto['descricao'] ?></p>
			
		<table>
			<thead>
				<tr>
					<th>Característica</th>
					<th>Detalhe</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Modelo</td>
					<td>Kimono Trançado</td>
				</tr>
				<tr>
					<td>Material</td>
					<td>Cardã</td>
				</tr>
				<tr>
					<td>Cores</td>
					<td>Preto, Verde, Azul</td>
				</tr>
				<tr>
					<td>Lavagem</td>
					<td>Lavar a mão</td>
				</tr>
			</tbody>
		</table>
		</div>
		</div>
		</div>


	<?php
		include ("rodape.php");
	?>
