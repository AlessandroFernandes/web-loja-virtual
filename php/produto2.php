	<?php
			$title_produtos = "Produtos Diversos";
			include ("cabecalho.php");
	?>
	
		<div class="container">

		<div class="row">
			<div class="col-md-8 center-block">
			
			<div class="produto">
		<h1>Kimono Dragão</h1>
		<p>por apenas R$ 120,00</p>

		<form action="checkout.php" method="POST">
			<fieldset class="cores">
				<legend>Escolha a cor:</legend>
				
				<input type="radio" name='cor' id="preto" value="preto" checked="true">
				<label for="preto">
					<img src="../image/kimonopreto.jpg" alt="kimono preto adulto">
				</label>

				<input type="radio" name="cor" id="verde" value="verde">
				<label for="verde">
					<img src="../image/kimonoverde.jpg" alt="kimono verde adulto">
				</label>

				<input type="radio" name="cor" id="branco" value="branco">
				<label for="branco">
					<img src="../image/kimonobranco.jpg" alt="kimono branco adulto">
				</label>
			</fieldset>
			<fieldset class="tamanhos">
			<legend>Escolha o Tamanho:</legend>
			
			<input type="range" min="36" max="46" value="42" step="2" name="tamanho" id="tamanho" class="taman"></input>
			</fieldset>

				<input type="submit" class="comprar" value="Comprar">

				<input type="hidden" name="nome" value="kimono dragao">
				<input type="hidden" name="preco" value="120,00">
		</form>
		</div>
		</div>

		<div class="row">
		<div class="col-md-12">	
		<div class="detalhes">
			<h2>Detalhes do produto</h2>

			<p>Esse é o melhor kimono de combate do mercado que você já viu. Excelente costura e anti-mofo, se tornando ideal para o dia a dia. Compre já e receba hoje mesmo pela nossa entrega a jato.</p>
		<table>
			<thead>
				<tr>
					<th>Característica</th>
					<th>Detalhe</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Modelo</td>
					<td>Kimono Trançado</td>
				</tr>
				<tr>
					<td>Material</td>
					<td>Cardã</td>
				</tr>
				<tr>
					<td>Cores</td>
					<td>Preto, Verde, Azul</td>
				</tr>
				<tr>
					<td>Lavagem</td>
					<td>Lavar a mão</td>
				</tr>
			</tbody>
		</table>

		</div>
		</div>
		</div>
		</div>
		

	<?php
		include ("rodape.php");
	?>
